<?php

namespace Drupal\module_core\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a BlockSimple block.
 *
 * @Block(
 *  id = "block_simple",
 *  admin_label = @Translation("Block simple"),
 * )
 */
class BlockSimple extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    // Traitement des données.
    $var1 = 'foo';
    $var2 = 'bar';

    return [
      '#type'   => 'markup',
      '#markup' => $var1 . ' ' .$var2,
    ];
  }

}
