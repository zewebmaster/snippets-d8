<?php

use Drupal\system\Entity\Menu;

use Drupal\Core\Link;
use Drupal\Core\Url;

// pour gérer les produits
use Drupal\commerce_order\Entity\OrderItem;


// pour gérer les user
use Drupal\user\Entity\User;



// pour gérer les entity
use Drupal\Core\Entity\EntityInterface;

// pour gérer les termes de taxo
use \Drupal\taxonomy\Entity\Vocabulary;
use \Drupal\taxonomy\Entity\Term;


// pour gérer les nodes
use Drupal\node\Entity\Node;






// pour gérer les formulaires
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;


// pour gérer les fields
use Drupal\Core\Field\Plugin\Field\FieldType\IntegerItem;


// manage file
use Drupal\file\Entity\File;
use Drupal\file\Plugin\Field\FieldType\FileFieldItemList;
