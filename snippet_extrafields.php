<?php

/**
 * Implements hook_entity_extra_field_info().
 *
 */
function portailweb_entity_extra_field_info() {

  // var
  $extra = [];

foreach (NodeType::loadMultiple() as $bundle)
  {
    $extra['node'][$bundle->Id()]['display']['my_own_pseudo_field'] = array(
      'label' => t('My own field'),
      'description' => t('This is my own pseudo-field'),
      'weight' => 100,
      'visible' => TRUE,
    );
  }
  return $extra;
}
