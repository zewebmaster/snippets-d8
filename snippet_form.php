<?php

// DAns le formulaire
$form['conf_numero'] = [
  '#type' => 'text_format',
  '#title' => 'Texte du bloc',
  '#default_value' => isset($config['conf_numero']) ? $config['conf_numero'] : '',
  '#rows' => 10,
  '#cols' => 10,
  '#required' => TRUE,
];


// Dans le build fom config
// Pour retourner un field texte wysiwyg

if (!empty($config['text_block'])) {
  $texte = $config['text_block']['value'];
  $format = $config['text_block']['format'];
}

return [
 '#theme' => 'bloc_numero_telephone',
 '#texte' => [
    '#type' => 'processed_text',
    '#text' => $texte,
    '#format' => $format,
  ],
 '#label_bouton' => $label_bouton,
 '#visuel' => '$visuel',
];
