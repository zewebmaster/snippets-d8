<?

// liste de tous les vocabulaires disponibles

use Drupal\taxonomy\Entity\Vocabulary;

    $vocabs = array();
    $vocabs_types = Vocabulary::loadMultiple();

    if (!empty($vocabs_types)) {
      foreach ($vocabs_types as $vocab_name => $vocab) {
        $vocabs[$vocab_name] = $vocab->label();
      }
      asort($vocabs);
    }


    $entity_type  = 'taxonomy_term';
    $vid          = 'random_display_front_prage';
    // chargement des termes du vocabulaire
    $terms        = \Drupal::entityTypeManager()->getStorage($entity_type)->loadTree($vid);
    $viewBuilder  = \Drupal::entityTypeManager()->getViewBuilder($entity_type);
    // traitement des termes
    if(!empty($terms))
    {
      // selection d'un terme au hasard
      $index  = rand(0 ,sizeof($terms)-1);
      $term   = $terms[$index];
      // chargement du terme
      $tid  = $term->tid;
      $term = Term::load($tid);
      // récupération des informations utiles
      // phrase d'accroche
      $field_accroche   = $term->get('field_accroche');
      $accroche         = $viewBuilder->viewField($field_accroche, 'default');
    }
