<?php

// liste de tous les menus disponibles

use Drupal\system\Entity\Menu;

    $menus = array();
    $menu_types = Menu::loadMultiple();

    if (!empty($menu_types))
    {
      foreach ($menu_types as $menu_name => $menu)
      {
        $menus[$menu_name] = $menu->label();
      }
      asort($menus);
    }

    kint($menus);


// toutes les entr�es d'un menu

    $menu_name  = 'account';
    $menu_tree  = \Drupal::menuTree();
    $parameters = $menu_tree->getCurrentRouteMenuTreeParameters($menu_name);
    $parameters->setMinDepth(0);
    //Delete comments to have only enabled links
    //$parameters->onlyEnabledLinks();

    $tree = $menu_tree->load($menu_name, $parameters);
    $manipulators = array(
      array('callable' => 'menu.default_tree_manipulators:checkAccess'),
      array('callable' => 'menu.default_tree_manipulators:generateIndexAndSort'),
    );
    $tree = $menu_tree->transform($tree, $manipulators);
    $list = [];

    foreach ($tree as $item)
    {
      $title  = $item->link->getTitle();
      $url    = $item->link->getUrlObject();
      $link   = Link::fromTextAndUrl($title, $url);
      $link   = $link->toRenderable();

      $items_user_account[] = array(
        'link' => $link,
      );
    }
