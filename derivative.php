<?php

namespace Drupal\portailweb\Plugin\Derivative;

use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides block plugin definitions for mymodule blocks.
 *
 * @see \Drupal\portailweb\Plugin\Block\DerivativeBlock
 */
class DerivativeBlock extends DeriverBase implements ContainerDeriverInterface {
  /**
   * @var EntityTypeManagerInterface $entityTypeManager.
   */
  protected $entityTypeManager;

  /**
   * Creates a ProfilMenuLink instance.
   *
   * @param $base_plugin_id
   * @param EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct($base_plugin_id, EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $base_plugin_id,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {

    $blocks = [];

    // on crée ici 5 blocs de manière arbitraire.
    // Mais il est possible de conditionner un traitement sur la base des données du site :
    // ex : taxonomie, catégorie d'article ...
    for($i=0;$i++;$i<5) {

      $id = \sprintf('block_derivative_%s', $i);
      $blocks[$id] = $base_plugin_definition;
      $blocks[$id]['admin_label'] = \sprintf("Block Dévative | %s", $i);
    }
    return $blocks;
  }
}
