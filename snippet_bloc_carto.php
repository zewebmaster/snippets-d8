<?php

namespace Drupal\account_core\Plugin\Block;

use Drupal\Core\Block\BlockBase;

// manage entityt
// use Drupal\Core\Entity\EntityInterface;
// manage user
use Drupal\user\Entity\User;
// manage file
use Drupal\file\Entity\File;
use Drupal\file\Plugin\Field\FieldType\FileFieldItemList;
use Drupal\image\Entity\ImageStyle;


/**
 * Provides a 'FrontPageMap' block.
 *
 * @Block(
 *  id = "bandeau_map",
 *  admin_label = @Translation("Bandeau OSMap account"),
 * )
 */
class BandeauMap extends BlockBase {

  /**
   * {@inheritdoc}
   *
   * Génére le bandeau carto de la front page à partir des données de l'utilisateur 
   */
  public function build() {

    // Chargement de l'utilisateur
    $account = User::load(23);
    // kint($account);

    // phone
    $field_phone  = $account->get('field_phone');
    $phone        = $field_phone->getValue();
    $phone        = reset($phone);
    // mail
    $mail         = $account->getEmail();
    // address
    $field_address  = $account->get('field_address');
    $address        = $field_address->getValue();
    $address        = reset($address);
    // map
    $field_map    = $account->get('field_map');
    $map          = $field_map->getValue();
    $map          = reset($map);
    $lat          = $map['lat'];
    $lon          = $map['lon'];

    // récupération des information à passer au theme
    $contact_phone    = $phone['value'];
    $organization     = $address['organization'];
    $thorougfare      = $address['address_line1'];
    $locality         = $address['postal_code'] . ' ' . $address['locality'];

    // Création de la map
    // initialisation
    $map  = leaflet_map_get_info('OSM Mapnik');
    $map['settings'] = array(
                        'dragging' => TRUE,
                        'touchZoom' => TRUE,
                        'zoom' => 15,
                        'scrollWheelZoom' => FALSE,
                        'doubleClickZoom' => TRUE,
                        'zoomControl' => TRUE,
                        'attributionControl' => TRUE,
                        'trackResize' => TRUE,
                        'fadeAnimation' => TRUE,
                        'zoomAnimation' => TRUE,
                        'closePopupOnClick' => TRUE,
                      );

    // popup
    $popup = $organization . '</br>';
    $popup .= $thorougfare . ' ' . $locality . '</br>';
    $popup .= $contact_phone . '</br>';
    $popup .= $mail;
    // coordnnées
    $features = array(
      array(
        'type' => 'point',
        'lat' => $lat,
        'lon' => $lon,
        'popup' => $popup,
        'leaflet_id' => 'leaflet_id'
      ),
    );

    // hauteur de la carte
    $height = '400px';
    // génération de la map
    $render_map = \Drupal::service('leaflet.service')->leafletRenderMap($map, $features, $height);


    // Construction de la réponse
    $build = array(
              '#theme'        => 'block_bandeau_map',
              '#map'          => $render_map,
          );

    return $build;
  }

}
