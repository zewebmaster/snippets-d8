<?php


// pour récupérer les données d'une requete
// Une valeur particulière ou toutes les valeurs

// $_POST
$value  = \Drupal::request()->request->get('key');
$params = \Drupal::request()->request->all();


// $_GET
$value  = \Drupal::request()->query->get('key');
$params = \Drupal::request()->query->all();


// traitement des résultas
$value =  array_key_exists('key', $params) ? $params['key'] : NULL;
