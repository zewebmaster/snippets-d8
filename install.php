<?php

/**
 * Implements hook_install_tasks().
 */
function portailweb_install_tasks(&$install_state) {

  $tasks = [];
  // déclaration des taches à réliser.
  $tasks['module_core_create_user'] = [];
  // $tasks['module_core_create_blocks'] = [];

  return $tasks;
}

/**
 * Installation task to create default users.
 */
function module_core_create_user() {

  $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
  $users = [ 'user1', 'user2', 'user3', 'user4' ];

  foreach($users as $user) {
    $user = \Drupal\user\Entity\User::create();
    $user->setUsername($user);
    $user->setEmail($user. '@mail.fr');
    $user->setPassword($user);
    $user->enforceIsNew();
    $user->set('langcode', $language);
    $user->set('preferred_langcode', $language);
    $user->set('preferred_admin_langcode', $language);
    $user->set('init', $user. '@mail.fr');
    $user->activate();
    $user->save();
  }
}


/**
 * Implements hook_schema()
 * Création de la structure de la base de donné
 */
function module_core_schema() {
  $schema['module_core_register'] = [
    'description' => 'Registre',
    'fields' => [
      'id' => [
        'type'      => 'serial',
        'not null'  => TRUE,
      ],
      'contact_name' => [
        'description' => 'nom de la personne contact',
        'type'        => 'varchar',
        'length'      => 255,
        'not null'    => TRUE,
        'default'     => '',
      ],
      'postal_code' => [
        'description' => 'Code postal',
        'type'        => 'varchar',
        'length'      => 5,
        'default'     => '',
      ],
      'email' => [
        'description' => 'contact mail',
        'type'        => 'varchar',
        'length'      => 255,
        'not null'    => TRUE,
        'default'     => '',
      ],
      'inscription' => [
        'description' => 'stock en cours',
        'type'        => 'int',
        'unsigned'    => TRUE,
        'not null'    => TRUE,
        'default'     => 0,
      ],
      'last_update' => [
        'description' => 'timestamp du dernier changement',
        'type'        => 'int',
        'default'     => 0,
      ],
    ],
    'unique keys' => [
      'product_id__localisation_id' => ['product_id', 'localisation_id'],
    ],
    'primary key' => ['id'],
  ];

  return $schema;
}
