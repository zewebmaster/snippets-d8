<?php

// pour gérer les url
use Drupal\Core\Url;

// pour gérer la configuration des champs
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;

// pour gérer la configuration des champ
use Drupal\field\Entity\FieldConfig;



// chargement du media
$file     = File::load($target_id);
$file_uri = $file->getFileUri();

// render image style
$image_rendered = ImageStyle::load('thumbnail')->buildUrl($file_uri);

// responsive image
$settings = array();
$settings['#theme']                     = 'responsive_image';
$settings['#uri']                       = $file_uri;
$settings['#height']                    = NULL;
$settings['#width']                     = NULL;
$settings['#responsive_image_style_id'] = 'configuration id on BO';
// génération de l'image
$picture = \Drupal::service('renderer')->render($settings);



// pour gérer l'image par défaut de l'utilisateur
$user          = User::load(1);
$picture = $user->get('user_picture')->getValue();

if(!empty($picture))
{
  $picture = reset($picture);
  $picture = $picture['target_id'];
  $file           = File::load($picture);
  $file_uri       = $file->getFileUri();
  $image_rendered = ImageStyle::load('thumbnail')->buildUrl($file_uri);
}
else
{
  $field          = FieldConfig::loadByName('user', 'user', 'user_picture');
  $default_image  = $field->getSetting('default_image');
  $file           = \Drupal::entityManager()->loadEntityByUuid('file', $default_image['uuid']);
  $file_uri       = $file->getFileUri();
  $image_rendered = ImageStyle::load('thumbnail')->buildUrl($file_uri);
}
